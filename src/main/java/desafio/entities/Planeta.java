package desafio.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name="planetas")
public class Planeta {
	
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	
	@NotEmpty(message="Favor informar o nome do planeta")
	private String nome;
	@NotEmpty(message="Favor informar o tipo de clima do planeta")
	private String clima;
	@NotEmpty(message="Favor informar o tipo de terreno do planeta")
	private String terreno;
	private Integer aparicoes;

}
