package desafio.ws.setup;

import javax.inject.Singleton;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Singleton
@Provider
public class ProcessingExceptionMapper implements ExceptionMapper<ProcessingException> {
	
    @Override
    public Response toResponse(ProcessingException e) {
        return Response.status(Status.BAD_REQUEST).entity("Conteúdo JSON inválido").build();
    }

}