package desafio.ws.setup;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Singleton;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Singleton
@Provider
public class ConstraintViolationMapper implements ExceptionMapper<ConstraintViolationException> {
	
	/** Wrapper para a formatação das críticas pegas pelo JSR-303 **/

    @Override
    public Response toResponse(ConstraintViolationException e) {
        List<String> messages = e.getConstraintViolations().stream()
            .map(ConstraintViolation::getMessage)
            .collect(Collectors.toList());

        return Response.status(Status.BAD_REQUEST).entity(new ErroValidacaoJSON(messages)).build();
    }
    
    @Getter
    @Setter
    @XmlRootElement
    private class ErroValidacaoJSON {
    	
    	private List<String> mensagens;
    	private Date data = new Date();
    	private int status = 400;
    	
    	public ErroValidacaoJSON(List<String> mensagens) {
    		this.mensagens = mensagens;
		}
    	
    }

}