package desafio.ws;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import desafio.data.PlanetaGateway;
import desafio.entities.Planeta;

@Path("planetas")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PlanetasWebService {
	
	@Inject
	private PlanetaGateway planetaGateway;
	
	@Inject 
	private SwapiFacade swapiFacade;
	
	@Context
	private UriInfo uriInfo;

	@POST
	@Path("/")
	public Response adicionarPlaneta(@Valid Planeta planeta)  throws Exception {
		
		/*
		 * Lendo o enunciado, fiquei na dúvida se a chamada da swapi deveria ser feita na inclusão (abrangeria futuros filmes) ou nas consultas (+performance).
		 * Optei por preservar a performance das consultas e deixei para o momento da inclusão.
		*/
		planeta.setAparicoes(swapiFacade.obterNumeroDeAparicoesDoPlanetaNosFilmes(planeta.getNome()));
		
		Planeta planetaAdicionado = planetaGateway.adicionarPlaneta(planeta);
		UriBuilder urlRecurso = uriInfo.getBaseUriBuilder().path(PlanetasWebService.class).path(planetaAdicionado.getId());
		return Response.created(urlRecurso.build()).build();
		
	}

	@GET
	@Path("/")
	public Response listarPlanetas(@QueryParam("nome") @DefaultValue("") String nome) throws Exception {

		boolean consultaPorNome = !"".equals(nome);

		if (consultaPorNome) {
			Planeta retornoPlanetaPorNome = planetaGateway.buscarPlanetaPorNome(nome);
			
			if(retornoPlanetaPorNome == null) {
				return Response.status(Status.NOT_FOUND).build();
			}
			
			return Response.ok(retornoPlanetaPorNome).build();
		} 
		else {
			return Response.ok(planetaGateway.listarPlanetas()).build();
		}

	}

	@GET
	@Path("/{id}")
	public Response buscarPlanetaPorId(@PathParam("id") String id) throws Exception {

		Planeta planetaConsultado = planetaGateway.buscarPlanetaPorId(id);

		if (planetaConsultado == null)
			return Response.status(Status.NOT_FOUND).build();

		return Response.ok(planetaConsultado).build();
	}

	@DELETE
	@Path("/{id}")
	public Response removerPlaneta(@PathParam("id") String id) throws Exception {
		
		Planeta planetaExcluir = planetaGateway.buscarPlanetaPorId(id);
		
		if (planetaExcluir == null)
			return Response.status(Status.NOT_FOUND).build();
		
		planetaGateway.removerPlaneta(planetaExcluir);
		return Response.ok().build();

	}

}
