package desafio.ws;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

@ApplicationScoped
public class SwapiFacade {
	
	private Client swapiClient;
	
	@PostConstruct
	public void init() {
		swapiClient = ClientBuilder.newClient();
	}

	public Integer obterNumeroDeAparicoesDoPlanetaNosFilmes(String nomePlaneta) {

		int aparicoes = 0;

		try {
			String jsonConsulta = swapiClient.target("https://swapi.co/api/planets/").queryParam("search", nomePlaneta).request(MediaType.APPLICATION_JSON).get(String.class);

			JSONObject objConsulta = new JSONObject(jsonConsulta);
			int resultados = objConsulta.getInt("count");

			if (resultados == 1)
				aparicoes = objConsulta.getJSONArray("results").getJSONObject(0).getJSONArray("films").length();
		}
		catch (Exception e) {
			/*
			 * logger.error("falha ao consultar dados do planeta na swapi para: " +
			 * nomePlaneta); logger.error(e);
			 */
		}
		return aparicoes;
	}
	
}
