package desafio.data;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import desafio.entities.Planeta;

@ApplicationScoped
public class PlanetaRepository implements PlanetaGateway {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Planeta> listarPlanetas() {
		return em.createQuery("select p from planetas p", Planeta.class).getResultList();
	}

	@Override
	@Transactional
	public Planeta adicionarPlaneta(Planeta planeta) {
		em.persist(planeta);
		return planeta;
	}

	@Override
	public Planeta buscarPlanetaPorId(String id) {
		return em.createQuery("select p from planetas p where p.id = :id", Planeta.class).setParameter("id", id)
				.getResultList().stream().findFirst().orElse(null);
	}

	@Override
	@Transactional
	public void removerPlaneta(Planeta planeta) {
		em.remove(em.contains(planeta) ? planeta : em.merge(planeta));
	}

	@Override
	public Planeta buscarPlanetaPorNome(String nome) {
		return em.createQuery("select p from planetas p where p.nome = :nome", Planeta.class).setParameter("nome", nome)
				.setMaxResults(1).getResultList().stream().findFirst().orElse(null);
	}

}
