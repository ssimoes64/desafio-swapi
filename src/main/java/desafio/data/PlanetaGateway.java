package desafio.data;

import java.util.List;

import desafio.entities.Planeta;

public interface PlanetaGateway {

	public List<Planeta> listarPlanetas();

	public Planeta adicionarPlaneta(Planeta planeta);

	public Planeta buscarPlanetaPorId(String id);

	public void removerPlaneta(Planeta planeta);

	public Planeta buscarPlanetaPorNome(String nome);

}